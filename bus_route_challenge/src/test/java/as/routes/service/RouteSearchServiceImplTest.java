package as.routes.service;

import as.routes.data.BusRoute;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;

public class RouteSearchServiceImplTest {

    private RouteSearchServiceImpl routeSearchService;
    private ArrayList<BusRoute> routeList;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        routeSearchService = new RouteSearchServiceImpl();
        routeList = new ArrayList<>();
    }

    @Test
    public void whenRoutesListEmptyThenFalse() {
        routeSearchService.init(routeList);
        boolean result = routeSearchService.routeExistsBetween(2, 3);
        assertThat(result, is(false));
    }

    @Test
    public void whenBusRouteListNullThenException() {
        routeSearchService.init(null);
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage(startsWith("List of bus routes"));
        routeSearchService.routeExistsBetween(1, 66);
    }

    @Test
    public void whenDirectRouteExistsThenTrue() {
        routeList.add(routeBetween(1, 6));
        routeSearchService.init(routeList);
        boolean result = routeSearchService.routeExistsBetween(1, 6);
        assertThat(result, is(true));
    }

    @Test
    public void whenDirectBusRouteNotExistsThenFalse() {
        routeList.add(routeBetween(1, 6));
        routeList.add(routeBetween(5, 7, 8, 19));
        routeSearchService.init(routeList);

        boolean result = routeSearchService.routeExistsBetween(1, 8);
        assertThat(result, is(false));
    }

    @Test
    public void whenThereExistsBusRouteWithChangesThenFalse() {
        routeList.add(routeBetween(1, 6));
        routeList.add(routeBetween(5, 7, 8, 19));
        routeList.add(routeBetween(8, 9, 10, 21));
        routeSearchService.init(routeList);

        boolean result = routeSearchService.routeExistsBetween(7, 21);
        assertThat(result, is(false));
    }

    @Test
    public void whenBusRoutesCarryUnsortedStopsThenFalse() {
        routeList.add(routeBetween(6, 1));
        routeList.add(routeBetween(7, 5, 8, 19));
        routeSearchService.init(routeList);

        boolean result = routeSearchService.routeExistsBetween(7, 8);
        assertThat(result, is(false));
    }

    private BusRoute routeBetween(int... stops) {
        return new BusRoute(stops);
    }


}