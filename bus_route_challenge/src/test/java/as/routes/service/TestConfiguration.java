package as.routes.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("Test")
@Configuration
@ComponentScan(basePackages = {"as.routes.service", "as.routes.service.util"})
class TestConfiguration {
}
