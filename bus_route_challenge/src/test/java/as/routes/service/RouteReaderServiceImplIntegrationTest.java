package as.routes.service;

import as.routes.data.BusRoute;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@ActiveProfiles("Test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class})
public class RouteReaderServiceImplIntegrationTest {

    @Autowired
    private RouteReaderService routeReaderService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
    }

    @Test
    public void havingFileInCorrectFormatWith10RoutesWhenReadThenCorrectRoutesParsed() throws IOException {
        List<BusRoute> busRoutes = routeReaderService.readRoutesFromFile("src/test/resources/correctFile");

        assertThat(busRoutes, hasSize(10));
        assertThat(Arrays.equals(busRoutes.get(0).getBusStops(),
                new int[]{17, 20, 24, 106, 140, 148, 150, 153, 160}),
                is(true));
    }

    @Test
    public void whenFileNotExistsThenException() throws IOException {
        expectedException.expect(FileNotFoundException.class);
        List<BusRoute> busRoutes = routeReaderService.readRoutesFromFile("some/non/existent/path");
    }

    @Test
    public void havingEmptyFileWhenReadThenEmptyListReturned() throws IOException {
        List<BusRoute> busRoutes = routeReaderService.readRoutesFromFile("src/test/resources/emptyFile");
        assertThat(busRoutes, empty());
    }
}