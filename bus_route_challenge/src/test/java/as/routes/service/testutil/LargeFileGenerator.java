package as.routes.service.testutil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class LargeFileGenerator {
    private static final int STATIONS_ON_ROUTE = 1000;
    private static final int BUS_ROUTES = 100_000;
    private static final int STATIONS = 1000_000;

    public static void main(String[] args) throws IOException {
        List<String> lines = new ArrayList<>();
        for (int i = 0; i < BUS_ROUTES; i++) {
            String line = new Random().ints(STATIONS_ON_ROUTE, 1, STATIONS).mapToObj(num -> Integer.toString(num)).collect(Collectors.joining(" "));
            lines.add(line.toString());
        }


        Files.write(Paths.get("largeFile.txt"), lines);
    }
}
