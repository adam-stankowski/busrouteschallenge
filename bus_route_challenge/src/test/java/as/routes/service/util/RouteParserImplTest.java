package as.routes.service.util;

import as.routes.data.BusRoute;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class RouteParserImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private RouteParser routeParser;

    @Before
    public void setUp() throws Exception {
        routeParser = new RouteParserImpl();
    }

    @Test
    public void whenRouteStringEmptyThenException() {
        expectedException.expect(IllegalArgumentException.class);
        routeParser.parseRoute(null);
    }

    @Test
    public void whenEmptyRouteStringThenException() {
        expectedException.expect(IllegalArgumentException.class);
        routeParser.parseRoute(null);
    }

    @Test
    public void whenBusRouteStringPassedThenCorrectBusRouteReturned() {
        BusRoute busRoute = routeParser.parseRoute("10 11 13 81 1234");

        assertThat(busRoute.getBusStops().length, is(4));
        assertThat(busStopsInRouteEqualTo(busRoute, 11, 13, 81, 1234), is(true));
    }

    @Test
    public void whenBusRouteStringsHaveUnorderedStopsThenTheyAreSorted() {
        BusRoute busRoute = routeParser.parseRoute("123 12 67 30 23");

        assertThat(busRoute.getBusStops().length, is(4));
        assertThat(busStopsInRouteEqualTo(busRoute, 12, 23, 30, 67), is(true));
    }

    private boolean busStopsInRouteEqualTo(BusRoute busRoute, int... stops) {
        return Arrays.equals(busRoute.getBusStops(), stops);
    }
}