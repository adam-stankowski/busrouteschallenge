package as.routes;

import as.routes.annotation.LogDebug;
import as.routes.data.BusRoute;
import as.routes.service.RouteReaderService;
import as.routes.service.RouteReaderServiceImpl;
import as.routes.service.util.RouteParser;
import as.routes.service.util.RouteParserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    @LogDebug
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
