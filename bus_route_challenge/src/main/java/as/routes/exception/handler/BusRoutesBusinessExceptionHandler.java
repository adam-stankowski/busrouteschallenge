package as.routes.exception.handler;

import as.routes.exception.BusRoutesBusinessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class BusRoutesBusinessExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {BusRoutesBusinessException.class})
    protected ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request) {
        String responseBody = "There was an issue " + ex.getMessage();
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

}
