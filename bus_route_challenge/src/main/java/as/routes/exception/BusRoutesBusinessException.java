package as.routes.exception;

public class BusRoutesBusinessException extends RuntimeException {
    public BusRoutesBusinessException(String messageText) {
        super(messageText);
    }
}
