package as.routes.controller;

import java.util.List;

import as.routes.annotation.LogDebug;
import as.routes.data.BusRoute;
import as.routes.exception.BusRoutesBusinessException;
import as.routes.response.BusRouteResponse;
import as.routes.service.RouteSearchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BusRoutesController {


    @Autowired
    private RouteSearchService routeSearchService;

    @RequestMapping(name = "/direct", method = RequestMethod.GET)
    @ResponseBody
    @LogDebug
    public ResponseEntity<BusRouteResponse> route(
            @RequestParam(value = "dep_sid") int start,
            @RequestParam(value = "arr_sid") int end) {

        if (start < 0 || end < 0 || start == end) {
            throw new BusRoutesBusinessException("Invalid request parameters");
        }

        boolean result = routeSearchService.routeExistsBetween(start, end);

        return new ResponseEntity<>(new BusRouteResponse(start, end, result), HttpStatus.OK);
    }
}
