package as.routes.service;

import as.routes.data.BusRoute;

import java.util.List;

public interface RouteSearchService {
    boolean routeExistsBetween(int startNode, int endNode);

    void init(List<BusRoute> busRoutes);
}
