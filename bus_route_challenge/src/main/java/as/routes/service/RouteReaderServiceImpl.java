package as.routes.service;

import as.routes.annotation.LogDebug;
import as.routes.data.BusRoute;
import as.routes.service.util.RouteParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class RouteReaderServiceImpl implements RouteReaderService {
    private static final Logger logger = LoggerFactory.getLogger(RouteReaderServiceImpl.class);

    @Autowired
    private RouteParser routeParser;

    @Override
    @LogDebug
    public List<BusRoute> readRoutesFromFile(String filename) throws IOException {
        List<BusRoute> result = new ArrayList<>();
        try (FileInputStream inputStream = new FileInputStream(filename);
             Scanner scanner = new Scanner(inputStream)) {

            skipFirstLine(scanner);
            while (scanner.hasNextLine()) {
                result.add(routeParser.parseRoute(scanner.nextLine()));
            }

            logger.debug("Read {} routes", result.size());

            // Scanner suppresses exceptions
            throwScannerExceptionIfExists(scanner);
            return result;
        }
    }

    private void skipFirstLine(Scanner scanner) {
        if (scanner.hasNextLine()) {
            scanner.nextLine();
        }
    }

    private void throwScannerExceptionIfExists(Scanner scanner) throws IOException {
        if (scanner.ioException() != null) {
            throw scanner.ioException();
        }
    }
}
