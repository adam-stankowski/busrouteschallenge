package as.routes.service.util;

import as.routes.data.BusRoute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class RouteParserImpl implements RouteParser {
    private static final Logger logger = LoggerFactory.getLogger(RouteParserImpl.class);
    private static final String ROUTES_NON_NULL = "Route cannot be null or empty";

    @Override
    public BusRoute parseRoute(String routeString) {
        logger.trace("Parsing single bus route {}", routeString);
        throwIfIllegalInput(routeString);

        return toBusRoute(routeString);
    }

    private BusRoute toBusRoute(String routeString) {
        int[] busStops = Arrays.stream(routeString
                .split("\\s+"))
                .skip(1)
                .mapToInt(Integer::parseInt)
                .sorted()
                .toArray();
        return new BusRoute(busStops);
    }

    private void throwIfIllegalInput(String routeStrings) {
        if (routeStrings == null || routeStrings.isEmpty()) {
            throw new IllegalArgumentException(ROUTES_NON_NULL);
        }
    }
}
