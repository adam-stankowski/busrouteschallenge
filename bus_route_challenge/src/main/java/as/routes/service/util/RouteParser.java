package as.routes.service.util;

import as.routes.data.BusRoute;

public interface RouteParser {
    BusRoute parseRoute(String routeString);
}
