package as.routes.service;

import as.routes.annotation.LogDebug;
import as.routes.data.BusRoute;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RouteSearchServiceImpl implements RouteSearchService {
    private static final Logger logger = LoggerFactory.getLogger(RouteSearchServiceImpl.class);

    private List<BusRoute> busRoutes;
    private boolean initialized;

    @Override
    public void init(List<BusRoute> busRoutes) {
        this.busRoutes = busRoutes;
        initialized = true;
    }

    @Override
    @LogDebug
    public boolean routeExistsBetween(int startStop, int endStop) {

        if(!initialized || busRoutes == null){
            throw new RuntimeException("List of bus routes not initialized");
        }

        return busRoutes.stream().anyMatch(route -> hasConnectionBetween(route, startStop, endStop));
    }

    private boolean hasConnectionBetween(BusRoute route, int startStop, int endStop) {
        logger.trace("hasConnectionBetween {}, stops {} and {}", route, startStop, endStop);
        int[] busStops = route.getBusStops();
        return busRouteContainsStartAndEndStop(startStop, endStop, busStops);
    }

    private boolean busRouteContainsStartAndEndStop(int startNode, int endNode, int[] busStops) {
        return Arrays.binarySearch(busStops, startNode) >= 0 && Arrays.binarySearch(busStops, endNode) >= 0;
    }
}
