package as.routes.service;

import as.routes.data.BusRoute;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface RouteReaderService {
    List<BusRoute> readRoutesFromFile(String filename) throws IOException;
}
