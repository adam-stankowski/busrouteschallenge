package as.routes.data;

public class BusRoute {
    private final int[] busStops;

    public BusRoute(int[] busStops) {
        this.busStops = busStops;
    }

    public int[] getBusStops() {
        return busStops;
    }
}
