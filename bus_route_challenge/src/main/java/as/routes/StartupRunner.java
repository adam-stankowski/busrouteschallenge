package as.routes;

import as.routes.annotation.LogDebug;
import as.routes.data.BusRoute;
import as.routes.service.RouteReaderService;
import as.routes.service.RouteSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StartupRunner implements CommandLineRunner{
    private static final Logger logger = LoggerFactory.getLogger(StartupRunner.class);

    @Autowired
    private  RouteReaderService routeReaderService;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    @LogDebug
    public void run(String... args) throws Exception {
        if(args.length < 1){
            logger.error("A file name with bus routes data needs to be provided in program argument");
            System.exit(1);
        }

        RouteSearchService routeSearchService = applicationContext.getBean(RouteSearchService.class);
        List<BusRoute> busRoutes = routeReaderService.readRoutesFromFile(args[0]);
        routeSearchService.init(busRoutes);
    }
}
