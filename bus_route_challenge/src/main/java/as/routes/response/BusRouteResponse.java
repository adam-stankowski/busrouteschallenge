package as.routes.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusRouteResponse {
    private final int departureStop;
    private final int arrivalStop;
    private final boolean directRouteExists;

    public BusRouteResponse(int departureStop, int arrivalStop, boolean directRouteExists) {
        this.departureStop = departureStop;
        this.arrivalStop = arrivalStop;
        this.directRouteExists = directRouteExists;
    }
    @JsonProperty("dep_sid")
    public int getDepartureStop() {
        return departureStop;
    }

    @JsonProperty("arr_sid")
    public int getArrivalStop() {
        return arrivalStop;
    }

    @JsonProperty("direct_bus_route")
    public boolean isDirectRouteExists() {
        return directRouteExists;
    }
}
