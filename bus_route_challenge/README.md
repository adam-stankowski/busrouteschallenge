# Bus Route Challenge Solution

### General
Application is based on SpringBoot for simplicity and created with TDD aproach in mind.

###Searching Algorithm and Data Structures
The main assumption that I made while creating it is:
- Direct bus route means that there exists a connection between two stops for same route id. Connection between two routes (routes with distinct ids) is irrelevant, so we return **false** in this case.

When problem is stated this way, a pretty simple algorithm and data structure is enough. The data structure simply holds an ordered array of stops for a single bus route and the algorithm uses a **binary search** to verify if
departure and arrival stops are within the same array. 
Worst case complexity of the search algorithm is **nlogn** and even for large data set it provided satisfying results when it comes to time of response. The search complexity is the same as in case of BinaryTree implementation.

The **advantage** is that it operates on primitives although better memory results can be achieved when the object memory overhead is removed and instead of splitting routes into BusRoute objects, we use a 2 dimensional array.

The **drawback** of this approach is that it may be problematic to expand the solution to bus station switches which were assumed out of scope. Also sorting stops on each bus route costs **nlogn** time. Avoiding this is an optimization which can definitely be done.
As the bus stops are sorted, the possibilities for further expansion of the solution to include say travel times between stops are reduced

###Reading bus route data
I assumed that application startup time is much less significant than time of response to api request and memory consumption.
Application parses bus route line into data structure directly to avoid storing strings in memory. Because bus route stops are sorted while parsing, this puts some drawback on startup time, as the reading algorithm
has **n^2logn complexity**, which is significantly higher than in case of Binary Tree which would be nlogn where n is the number of lines read. This compromise is made to save memory